function traj_corrected = cleanCMUdataset()
% cleanCMUdataset - removes erroneous data from CMU dataset 
% 
%
% Syntax:  
%    traj_corrected = findTrackingErrors(traj,correctionFlag)
%
% Inputs:
%    traj - recorded trajectory of a marker
%    correction_flag - 1 if trajectory should be automatically
%
% Outputs:
%    t_violation - time relative to the start time of first violation; inf
%    is returned if no violation took place.
%
% Example: 
%
% 
% Author:       Matthias Althoff
% Written:      08-March-2019
% Last update:  ---
% Last revision:---


%------------- BEGIN CODE --------------

% load data
load CMU_data_for_Science

% number of movements
nrOfMovements = length(ALLMOVEMENTS(:,1))-1;

%for iMovement = 1:nrOfMovements
for iMovement = 114:114

    % extract trajectories of all markers
    fullTraj = ALLMOVEMENTS{iMovement+1,2};

    % remove erroneous data
    corrTraj = errorSearch(fullTraj);
    
    % rewrite data
    ALLMOVEMENTS{iMovement+1,2} = corrTraj;
    
    iMovement
end

% save corrected data
save CMU_data_for_Science_corrected ALLMOVEMENTS DESCRIPTIONS ORDER

%------------- END OF CODE --------------